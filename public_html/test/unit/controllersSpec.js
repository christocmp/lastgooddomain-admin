/**
 * Created by christo on 19/09/2014.
 */

describe('DomainListCtrl', function(){

    beforeEach(module('domainApp'));

    it('should create domain model with an array of domains', inject(function($controller) {
        var scope = {},
            ctrl = $controller('DomainListCtrl', {$scope:scope});

        expect(scope.phones.length).toBe(3);
    }));

});
