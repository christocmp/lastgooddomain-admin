<?php
namespace DomainScraper;
/**
 * Created by IntelliJ IDEA.
 * User: christo
 * Date: 19/09/2014
 * Time: 15:14
 *
 * Shoot out JSON data for angularJS
 */

require_once '../config.php';
//echo CLASS_PATH . 'class.domain.php';
require_once CLASS_PATH . 'class.domain.php';

$domain = new Domain();
$domain->setId($_GET['domainId']);

echo json_encode($domain->getDomain());

?>