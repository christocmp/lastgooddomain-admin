<?php
namespace DomainScraper;

require_once '../config.php';
require_once CLASS_PATH . 'class.domain.php';

use \DomainScraper\Domain;
$domain = new Domain();

if (isset($_GET['fromDate']) && !empty($_GET['fromDate']) ){ //PDO is used but this is sloppy!
    $fromDate = $_GET['fromDate'];
}

if (isset($_GET['pagerank']) && !empty($_GET['pagerank'])){
    $minPagerank = $_GET['pagerank'];
}

if (isset($_GET['page']) && !empty($_GET['page'])){
    $page = $_GET['page'];
}

$results = $domain->getInterestingDomainsPaginated($fromDate, $minPagerank, $page);
$total = $domain->getTotalResults();

$final[] = $results;
$final[] = $total;
echo json_encode($final)

?>