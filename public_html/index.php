<!doctype html>
<html lang="en" ng-app="domainApp">
<head>
    <meta charset="utf-8">

    <title ng-bind-template="Domain List: {{query}}">Domain List</title>

    <script type="text/javascript" src="bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="bower_components/jquery-ui/jquery-ui.js"></script>
    <script src="bower_components/angular/angular.js"></script>
    <script src="bower_components/angular-route/angular-route.js"></script>
    <script src="bower_components/angular-utils-pagination/dirPagination.js"></script>

    <script src="app/js/app.js"></script>

    <script src="app/js/controllers.js"></script>
    <script src="app/js/filters.js"></script>


    <link rel="stylesheet" href="stylesheets/app.css">
    <script src="js/app.js"></script>
    <base href="/">
</head>
<body lang="en" ng-app="domainApp">

<div ng-view></div>

</hr>

</body>
</html>