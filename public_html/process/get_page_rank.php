<?php
namespace DomainScraper;
set_time_limit(86100); //86100 = 24 hours in seconds
require_once '../config.php';
require_once CLASS_PATH . 'class.domain.php';
require_once CLASS_PATH . 'GooglePageRank3.php';

$log_filename = "get_page_rank.log";

$domain = new Domain();
$domain->setCutoffDate(1); //1 days past - default tomorrow other wise days in future i.e don't look at domains before tomorrows date as out of auction

echo "getting all to domains lacking pagerank info\n";

// new pr class
$pagerank = GooglePR::get('wikipedia.org');
echo "wikipedia pr is $pagerank";
sleep(1);
// end new pr class

flush();

$domain_list = $domain->getAllToPagerank();



/*
$seoStats = new SEOstats();

$seoStats->domain = "http://www.wikipedia.org";
    echo $seoStats->PageRank(); 
*/
$row = 0;
$hundred_count = 0;

echo "beginning PR inspection loop\n";

file_put_contents($log_filename, date('l jS \of F Y h:i:s A') . PHP_EOL);
file_put_contents($log_filename, date('l jS \of F Y h:i:s A') . PHP_EOL, FILE_APPEND);
flush();
foreach ($domain_list as $domain_row){
	//check pr is working ok
	if ($hundred_count == 100 || $hundred_count == 0){
		
		$hundred_count = 1;
		echo "\nChecking wikipedia PR\n";
		flush();
		if (GooglePR::get('wikipedia.org') < 1){
			sleep(1);
			file_put_contents($log_filename, date('l jS \of F Y h:i:s A') . PHP_EOL, FILE_APPEND);
			echo "Pagerank giving false readings\n";
			flush();
			die();
		}
	}
	
	/*if ($hundred_count == 4){
		echo 'have PR checked 400 ending';
		die();
	}*/
	
	$hundred_count++;
	$row++;
    //$seoStats->domain = "http://www." . $domain_row['domain_name'];
	$rank = GooglePR::get($domain_row['domain_name']);
	file_put_contents($log_filename, $domain_row['domain_name'] . PHP_EOL, FILE_APPEND);

	echo $domain_row['domain_name'];
	echo $rank . "\n\n";
	$domain->setId($domain_row['id']);
	
	if ($rank < 0 || empty($rank)){ $rank = 0;}
	
	$domain->setPageRank($rank);
	flush();
	sleep(1);
	

}
echo "\nending PR inspection loop\n";
flush();

?>
