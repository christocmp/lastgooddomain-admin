<?php
namespace DomainScraper;
ini_set("memory_limit","256M");
ini_set("max_execution_time","0");
require_once '../config.php';
require_once CLASS_PATH . 'class.domain.php';

echo CLASS_PATH;
$domain = new Domain();

echo "importing lists \n";
flush();

/*
echo "removing expired \n";
flush();
$domain->removeExpired(); //Christo 24/10/2014 - has not been working query looks fine in new class disabled for now
*/



//

$domain_feed_files = array();
echo "reading snapnames files \n";
flush();
$directory = dir(DOC_ROOT . 'process') or die($php_errormsg);

while (false !== ($f = $directory->read())) {
    if (is_file($directory->path.'/'.$f) && strpos($f, '.txt')) {
        $domain_feed_files[] = $f;
    }
}
$directory->close();

print_r ($domain_feed_files);

echo "begining database import \n";
flush();
$row = 1;
foreach ($domain_feed_files as $file){
	$handle = fopen($file, "r");
	while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {

		
		if ($row == 1){
			$row++;
			continue;
		}

		$domain_name = trim($data[0]);
//		echo "$domain_name \n";
//                flush();

		if ($domain->isDuplicate($domain_name)){
//                        echo "duplicate \n";
//                        flush();
			continue;
		}
		
		$current_bid = $data[1];
		$phpdate = strtotime( $data[2] );
		$join_date =   date( 'Y-m-d', $phpdate );
		
		if ($data[4] == ''){
			$bidders = 0;
		}
		else{
			$bidders = $data[4];
		}
		
		$seller =      trim($data[5]);
		$extension =   trim($data[6]);
		$length =      $data[7];
		$keywords    = trim($data[8]);
		$word_count  = trim($data[9]);
		$categories  = trim($data[10]);
		
		if ($data[11] == 'Yes'){
			$hyphens = 1;
		}
		else{
			$hyphens = 0;
		}
		
		if ($data[12] == 'Yes'){
			$numbers = 1;
		}
		else{
			$numbers = 0;
		}

		$auction_type = trim($data[13]);
		
		$category1 = '';
		$category2 = '';
		$category3 = '';
		
		$pieces = explode(" ", $categories);
		
		if (isset($pieces[0]) && !empty($pieces[0])){
			$category1 = $pieces[0];
			if (isset($pieces[1]) && !empty($pieces[1])){
				$category2 = $pieces[1];
			}
			if (isset($pieces[2]) && !empty($pieces[2])){
				$category3 = $pieces[2];
			}
		}
		
		$domain->addDomain($domain_name, $current_bid, $join_date, $bidders, $seller, $extension, $length,$keywords,$word_count,
		 $category1,$category2,$category3,$hyphens,$numbers,$auction_type);
		
//echo "added \n\n";
//flush();

$row++;

	}
	fclose($handle);
	
	
}
echo 'import complete\n';
?>
