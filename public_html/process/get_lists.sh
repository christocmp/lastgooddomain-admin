#!/bin/bash
$DIR="/srv/www/admin.lastgooddomain.com/lastgooddomain-admin/public_html/process/"

echo "changing directory"
cd $DIR
echo "getting exclusive list"
rm file_dl.sn?file=snpexpiringexlusivelist.zip
rm snpdeletinglist.txt
rm snpexpiringexlusivelist.txt
rm snpmostactivelist.txt
wget --no-check-certificate https://www.snapnames.com/file_dl.sn?file=snpexpiringexlusivelist.zip
unzip -o file_dl.sn?file=snpexpiringexlusivelist.zip
echo "getting delete list"
rm file_dl.sn?file=snpdeletinglist.zip
wget --no-check-certificate https://www.snapnames.com/file_dl.sn?file=snpdeletinglist.zip
unzip -o file_dl.sn?file=snpdeletinglist.zip
echo "getting in auction list"
rm file_dl.sn?file=snpmostactivelist.zip
wget --no-check-certificate https://www.snapnames.com/file_dl.sn?file=snpmostactivelist.zip
unzip -o file_dl.sn?file=snpmostactivelist.zip
