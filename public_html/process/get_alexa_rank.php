<?php
namespace DomainScraper;
ini_set("memory_limit","128M");
ini_set("max_execution_time","0");
require_once '../config.php';
require_once CLASS_PATH . 'class.domain.php';
//require_once CLASS_PATH . 'alexa_functions.php';
require_once CLASS_PATH . 'class.alexa.php';

$domain = new Domain();
$domain->setCutoffDate(1); //default tomorrow

$domain_list = $domain->getAllToAlexarank();

$row = 0;
/*
print_r($domain_list);
die();
echo 'wikipedia alexa rank: ' . Alexa::getrank('wikipedia.org');
die();
*/
foreach ($domain_list as $domain_row){

	$row++;
	
	$rank = Alexa::getrank($domain_row['domain_name']);
	echo $domain_row['domain_name'];
	echo $rank . '<br />';
	$domain->setId($domain_row['id']);
	
	if ($rank <= 0){ $rank = 0;}
	
	$domain->setAlexaRank($rank);
	flush();
	usleep(1000);
}

?>
