<?php

require_once '../config.php';
require_once CLASS_PATH . 'GooglePageRank3.php'; 
// some sample and test urls 
$urls = array('http://www.facebook.com', # site without trailing slash 
     'http://www.google.com/ads/publisher/', # site with subpage 
     'http://asdf.com/asdf.html', # site without www subdomain and with page that doesnt exist 
     'http://ezinearticles.com/?cat=Business', # site with query only 
     'http://staad.pro-online-tutorial.fyxm.net/', # new site with pr 0 
     'http://warriorforumwsoreviews.blogspot.com/', # new site without pr 
     'http://n-o-t-a-v-a-i-l-a-b-l-e.net/'); # site that doesnt exist 
// print out pr check results 
//echo '<pre>'.print_r(GooglePR::bulk($urls), 1).'</pre>'; 

$pagerank = GooglePR::get('wikipedia.org');
echo "wikipedia pr is $pagerank";

/*
$domain = "http://www.wikipedia.org"; // Enter your domain here.
echo "testing for pr";
$rank = GooglePageRankChecker::getRank("wikipedia.org"); // returns "5"
*/   
?>