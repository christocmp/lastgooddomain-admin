<!doctype html>
<html lang="en" ng-app="domainApp">
<head>
    <meta charset="utf-8">

    <title ng-bind-template="Google Phone Gallery: {{query}}">Google Phone Gallery</title>

    <script src="bower_components/angular/angular.js"></script>
    <script src="app/js/controllers.js"></script>
    <script src="app/js/app.js"></script>
    <script src="bower_components/angular-route/angular-route.js"></script>
    <script src="bower_components/angular-utils-pagination/dirPagination.js"></script>
    <script src="app/js/filters.js"></script>
</head>
<body lang="en" ng-app="domainApp">

<h1>11</h1>
<p>11 is about building a restful interface maybe over the top for our needs here</p>
<p>12 deals with animation again no use here</p>

<h1>6</h1>

<div ng-view></div>

</hr>

<h1>5</h1>
<div lang="en" ng-controller="DomainListCtrl">
Search: <input ng-model="query">
Sort by:
<select ng-model="domainProp">
    <option value="domain_name">Alphabetical</option>
    <option value="id">Newest</option>
    <option value="-id">Oldest</option>
</select>
Limit: <input ng-model="limit">



<ul class="domains">
    <li ng-repeat="domain in domains | filter:query | orderBy:domainProp">
        <span>{{domain.domain_name}}</span>
        <a href="#/phones/{{domain.id}}">{{domain.domain_name}}</a>
        <a href="#/phones/{{domain.id}}" class="thumb"><img ng-src="{{domain.imageUrl}}"></a>
    </li>
</ul>
<h2>Debug</h2>
<pre>{{domains | json}}</pre>
</p>
</div>
</hr>

<div lang="en" ng-controller="PhoneListCtrl">
<h1>4</h1>

Search: <input ng-model="query">
Sort by:
<select ng-model="orderProp">
    <option value="name">Alphabetical</option>
    <option value="age">Newest</option>
    <option value="-age">Oldest</option>
</select>


<ul class="phones">
    <li ng-repeat="phone in phones | filter:query | orderBy:orderProp">
        <span>{{phone.name}}</span>
        <p>{{phone.snippet}}</p>
    </li>
</ul>
<h2>Debug</h2>
{{orderProp}}
</p>

<hr />

<h1>3</h1>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            <!--Sidebar content-->

            Search: <input ng-model="query">

        </div>
        <div class="col-md-10">
            <!--Body content-->

            <ul class="phones">
                <li ng-repeat="phone in phones | filter:query">
                    {{phone.name}}
                    <p>{{phone.snippet}}</p>
                </li>
            </ul>

        </div>
    </div>
</div>

<p>
    <h2>Debug</h2>
    {{query}}
</p>

<hr />
<h1>1</h1>
<p>Nothing here {{'yet' + '!'}}</p>

<p>8 Megabytes is actually  {{ 1024 * 8 }}</p>


<hr />
<h1>2</h1>
<ul>
    <li ng-repeat="phone in phones">
        {{phone.name}}
        <p>{{phone.snippet}}</p>
    </li>
</ul>
<p>Total number of phones: {{phones.length}}</p>

<p><h1>{{name}}</h1></p>

<table>

    <tr ng-repeat="i in [0, 1, 2, 3, 4, 5, 6, 7]"><td ng-repeat="i in [0, 1, 2, 3, 4, 5, 6, 7,8,9]">{{i +1}}</td></tr>
</table>
</div>

</body>
</html>