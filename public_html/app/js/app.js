var domainApp = angular.module('domainApp', [
    'ngRoute',
    'angularUtils.directives.dirPagination',
    'domainControllers',
    'domainFilters'
]);


domainApp.directive('myDatepicker', function () {
    return {
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                element.datepicker({
                    showOn:"both",
                    changeYear:true,
                    changeMonth:true,
                    dateFormat:'yy-mm-dd',
                    maxDate: new Date(),
                    yearRange: '2000:2015',
                    onSelect:function (dateText, inst) {
                        ngModelCtrl.$setViewValue(dateText);
                        scope.$apply();
                    }
                });
            });
        }
    }
});


//bower was used to install this routing is not in base ng
domainApp.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
        $routeProvider.
            when('/domains', {
                templateUrl: 'app/partials/domain-list.html',
                controller: 'DomainListCtrl'
            }).
            when('/domain/:domainId', {
                templateUrl: 'app/partials/domain-detail.html',
                controller: 'DomainDetailCtrl'
            }).
            otherwise({
                templateUrl: 'app/partials/domain-list.html',
                controller: 'DomainListCtrl'
                //redirectTo: '/domains'
            });
        // use the HTML5 History API
        $locationProvider.html5Mode(true);
    }]);