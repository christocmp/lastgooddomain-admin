var domainControllers = this.angular.module('domainControllers', []);

function getTodaysDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    today = yyyy + '-' + mm + '-' + dd;
    return(today);
}

// [] are needed to prevent minify ruining things. Angular derives stuff from whats passed to controller
domainControllers.controller('DomainListCtrl', ['$scope', '$http' ,
    function ($scope, $http) {
        $scope.domainProp = 'id';
        $scope.pagerank = 1;
        $scope.mDate = getTodaysDate();
        $scope.noResults = true;
        $scope.processing = false;
        $scope.domainsPerPage = 5; // this should match however many results your API puts on one page
        $scope.processingMessage = '';

        $scope.pagination = {
            current: 1
        };

        $scope.doSearch = function() {
                $scope.processing = true;
                $scope.noResults = true;
                $scope.statusMessage = '';
                $scope.processingMessage = 'Getting results.';
                getResultsPage(1);
        };

        $scope.pageChanged = function(newPage) {
            getResultsPage(newPage);
        };

        function getResultsPage (pageNumber) {
                // do whatever you were going to do
                $http.get('../json/domains.php?pagerank=' + $scope.pagerank +'&fromDate=' + $scope.mDate +'&page=' + pageNumber).success(function(data) {

                    $scope.domains = data[0];
                    $scope.totalDomains = data[1];
                    if ($scope.totalDomains > 0){
                        $scope.noResults = false;
                        $scope.statusMessage = '';
                    }
                    else{
                        $scope.statusMessage = 'There are no results to display.';
                    }
                    $scope.processing = false;
                });
        }

}]);


domainControllers.controller('DomainDetailCtrl', ['$scope', '$routeParams', '$http',
    function($scope, $routeParams, $http) {
        $scope.domainId = $routeParams.domainId;
        $http.get('../json/domain.php?domainId=' + $routeParams.domainId).success(function(data) {
            $scope.domain = data;
        });

        $scope.hello = function() {
            this.alert('Hello ' + ($scope.domainId || 'world') + '!');
        };
    }]);