<?php
namespace DomainScraper;
/**
�* PHP Class to get a website Alexa Ranking
�* @author http://www.paulund.co.uk
�*
�*/
class alexa{

public function getrank($domain){
$url = "http://data.alexa.com/data?cli=10&dat=snbamz&url=".$domain;
$ch = curl_init();
//Set curl to return the data instead of printing it to the browser.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,2);
curl_setopt($ch, CURLOPT_URL, $url);
$data = curl_exec($ch);
curl_close($ch);
$xml = new SimpleXMLElement($data);
$popularity = $xml->xpath("//POPULARITY");
$rank = (string)$popularity[0]['TEXT'];
return $rank;
}

}
?>