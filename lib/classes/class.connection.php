<?php
namespace DomainScraper;
/**
 * Class Connection
 *
 * Basic connection class using singleton design pattern
 */
class Connection

{
    private $_handle = null;

    private function __construct()
    {
        $this->_handle = new \PDO('mysql:host=localhost;dbname=domains', 'root', ':H7/Ca44dt', array(
            \PDO::ATTR_PERSISTENT => true));
    }

    public function __destruct()
    {
        $this->_handle = null; //close the connection as it is persistent
    }

    /**
     * @return Connection
     *
     * returns a new connection object or returns existing one
     */

    public static function get()
    {
        static $db = null; // only initialised in first call

        if ($db == null) {
            $db = new Connection();
        }
        return $db;
    }


    /**
     * @return null
     */
    public function handle()
    {
        return $this->_handle;
    }

}

?>